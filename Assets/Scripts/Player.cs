﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MonoBehaviour {

	public Text healthText;
	public AudioClip JumpSound;
	
	[HideInInspector] public bool facingRight = true;
	[HideInInspector] public bool jump = false;
	public float moveForce = 365f;
	public float maxSpeed = 5f;
	public float jumpForce = 1000f;
	public Transform groundCheck;
	
	
	private bool grounded = false;
	private Animator anim;
	private int playerHealth = 0;
	private int healthPerCoin = 2;
	private Rigidbody2D rb2d;
	

	void Awake () 
	{
		anim = GetComponent<Animator>();
		healthText.text = "Health: " + playerHealth;
		rb2d = GetComponent<Rigidbody2D>();
	}

	void Update () 
	{
		grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
		
		if (Input.GetButtonDown("Jump") && grounded)
		{
			jump = true;
		}
	}
	
	void FixedUpdate()
	{
		float h = Input.GetAxis("Horizontal");
		
		anim.SetFloat("Speed", Mathf.Abs(h));
		
		if (h * rb2d.velocity.x < maxSpeed)
			rb2d.AddForce(Vector2.right * h * moveForce);
		
		if (Mathf.Abs (rb2d.velocity.x) > maxSpeed)
			rb2d.velocity = new Vector2(Mathf.Sign (rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);
		
		if (h > 0 && !facingRight)
			Flip ();
		else if (h < 0 && facingRight)
			Flip ();
		
		if (jump)
		{
			anim.SetTrigger("Jump");
			rb2d.AddForce(new Vector2(0f, jumpForce));
			jump = false;
			SoundController.Instance.PlaySingle(JumpSound);
		}
	}
	
	
	void Flip()
	{
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	private void OnTriggerEnter2D(Collider2D objectPlayerCollidedWith)
	{
		if(objectPlayerCollidedWith.tag == "Coin") 
		{
			playerHealth += healthPerCoin;
			healthText.text = "+" + 2 + " Health\n" + "Health: " + playerHealth;
			objectPlayerCollidedWith.gameObject.SetActive(false);
 		}
	}	
}