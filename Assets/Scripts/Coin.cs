﻿using UnityEngine;
using System.Collections;

public class Coin : MonoBehaviour {

	public AudioClip CoinSound;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject.CompareTag ("Player"))
		{
			Destroy(gameObject);
			SoundController.Instance.PlaySingle(CoinSound);
		}

	}
}
